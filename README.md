# Mandelbrot-Tryout

A demonstration of a simple Python-script to generate images of the Mandelbrot-set.

Published [by a wish of @M@nerdculture.de](https://wue.social/web/statuses/107781502423182983).