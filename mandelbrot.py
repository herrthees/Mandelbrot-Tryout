from PIL import Image, ImageDraw
import datetime

# timestamp for filename of image
timestamp = datetime.datetime.today().strftime('%Y%m%d%H%M%S')

# How deep should every point be iterated
maxIterations = 80

# Aspect ratio of the image, the pixel-width and the calculated height
aspectRatio = 4/3
pixel_width = 320
pixel_height = int(pixel_width/aspectRatio)

# complex numbers have a real and an imaginary part. 
# Drawn in a Cartesian coordinate system the real part is 
# the x-axis, the imaginary part the y-axis. 
# realStart and imaginaryStart are the starting point of
# the "viewing window" of the Mandelbrot script.

# classic start: the Apfelmännchen
# zoomFactor = 3
# realStart = -2
# imaginaryStart = -1.5

zoomFactor = 0.007 # > 0, smaller is more zoom
realStart = -0.276  # Smaller is "move to the left"
imaginaryStart = -0.845  # Smaller ist "move up"

realEnd = realStart + zoomFactor
imaginaryEnd = imaginaryStart + zoomFactor

palette = [(150, 20, 20), (170, 30, 20), (180, 40, 20), (190, 50, 20), (200, 60, 40), (225, 81, 74), (251, 165, 92), (254, 232, 153),
           (236, 247, 162), (216, 227, 142), (206, 217, 122), (161, 217, 144), (131, 191, 164), (100, 130, 164), (71, 159, 179), (0, 0, 0)]
backgroundColor = (0, 0, 0)

# Initialize the image
image = Image.new('RGB', (pixel_width, pixel_height), backgroundColor)
draw = ImageDraw.Draw(image)

# Looping through the pixels of the image
for x in range(0, pixel_width):
    for y in range(0, pixel_height):
        
        # thank you Python for the numeric type "complex"
        # calculate the complex number for the current pixel in the loop
        c = complex(
            realStart + (realEnd - realStart) / pixel_width * x,
            imaginaryStart + (imaginaryEnd - imaginaryStart) / pixel_height * y 
        )

        # The Mandelbrot algorithm
        z = 0
        iterations = 0
        while abs(z) <= 2 and iterations < maxIterations:
            z = z*z + c
            iterations += 1
        
        # simple coloring: reduce iterations to a range from 0 to 255
        # draw pixel with the corresponding color-tuple in the palette-list
        color = int(255 * iterations / maxIterations)
        paletteIndex = int(color/(255/len(palette))-1)
        draw.point([x, y], palette[paletteIndex])

image.save('madelbrot_'+timestamp+'.png', 'PNG')